# README #

This Program runs a game of **siete y medio**, a Spanish card game.
The player starts with $100, and can bet against a dealer.

**BUG** - Final commit on Branch: WORKINGexperimental does not work. I tried adding post-game alerts which broke the program. Please help fix.


### Features ###

* Accounts for bets, winnings, and players' total money
* Prints record of all hands and bets to gamelog.txt
* ~~post-game alerts~~ NOT WORKING

### How to Play ###

1. Place Bet
2. Press __y__ or __n__ to be dealt additional cards
3. Try to get as close to 7.5 without going over!